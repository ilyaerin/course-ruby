require 'drb'

storage = DRbObject.new nil, 'druby://:9000'

puts "add 'abc': #{storage.add 'abc'}"
puts "add 'asbrr,asssss': #{storage.add 'asbrr,asssss'}"
puts "add 'wRoNG_StrinG': #{storage.add 'wRoNG_StrinG'}"

puts "storage: #{storage}"
puts "abc: #{storage.contains? 'abc'}"
puts "asb: #{storage.contains? 'asb'}"
puts "as: #{storage.contains? 'as'}"
puts "ass: #{storage.contains? 'ass'}"
puts "asss: #{storage.contains? 'asss'}"
puts "a: #{storage.contains? 'a'}"

puts "asb: #{storage.find 'asb'}"
puts "asd: #{storage.find 'asd'}"
puts "ass: #{storage.find 'ass'}"
puts "abc: #{storage.find 'abc'}"
puts "ab: #{storage.find 'ab'}"
puts "xafax: #{storage.find 'xafax'}"