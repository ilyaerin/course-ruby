require 'drb'
require 'storage'
require 'time'

class StorageDRB
  def initialize
    @storage = Storage::Storage.new
    @storage.load_from_file '../base_storage.txt'
  end

  def add(string)
    begin
      @storage.add(string)
    rescue ArgumentError => ex
      log "#{ex.message} occurred for add with params '#{string}'"
    end
  end

  def contains?(path)
    @storage.contains?(path)
  end

  def find(path)
    begin
      @storage.find(path)
    rescue ArgumentError => ex
      log "#{ex.message} occurred for find with params '#{path}'"
    end
  end

  private

    def raise_error(message)
      log(message)
      message
    end
end

def log(string, type = :error)
  puts "#{type.upcase} #{DateTime.now}: #{string}"
end

DRb.start_service 'druby://:9000', StorageDRB.new

log "Server running at #{DRb.uri}", :info

trap("INT") { DRb.stop_service }
DRb.thread.join