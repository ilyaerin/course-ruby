require 'sinatra'
require 'json'
require 'storage'

storage = Storage::Storage.new
storage.load_from_file 'base_storage.txt'

set :server, :thin
set :bind, '0.0.0.0'
set :port, 3000
set :logging, true

get '/add/:string' do
  content_type :json
  begin
    {result: storage.add(params[:string])}.to_json
  rescue ArgumentError => ex
    raise_error("#{ex.message} occurred for add with params  '#{params[:string]}'")
  end
end

get '/contains/:string' do
  content_type :json
  {result: storage.contains?(params[:string])}.to_json
end

get '/find/:string' do
  content_type :json
  begin
    {words: storage.find(params[:string])}.to_json
  rescue ArgumentError => ex
    raise_error("#{ex.message} occurred for find with params '#{params[:string]}'")
  end
end

get '*' do
  content_type :json
  raise_error("#Invalid request: '#{params}'")
end


def raise_error(message)
  logger.error(message)
  {error: message}.to_json
end