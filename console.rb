require 'storage'

# storage = Storage::Storage.new 'ab,a,xafaxx,asss,asb,asdff,astt,asbf'
storage = Storage::Storage.new ''
# storage.load_from_file 'test'
storage.load_from_zip 'test.zip'

storage.add 'abc'
storage.add 'asbrr'
# storage.add 1

puts "storage: #{storage}"
puts "abc: #{storage.contains? 'abc'}"
puts "asb: #{storage.contains? 'asb'}"
puts "as: #{storage.contains? 'as'}"
puts "ass: #{storage.contains? 'ass'}"
puts "asss: #{storage.contains? 'asss'}"
puts "a: #{storage.contains? 'a'}"

puts "asb: #{storage.find 'asb'}"
puts "asd: #{storage.find 'asd'}"
puts "xaff: #{storage.find 'xaff'}"
puts "xafax: #{storage.find 'xafax'}"
# storage.find 'a'

storage.safe_to_file 'test'
storage.safe_to_zip 'test.zip'
